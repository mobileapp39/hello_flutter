import 'package:flutter/material.dart';
void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String thaiGreeting = "สวัสดี Flutter";
String frenchGreeting = "Bonjour flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //Scaffold Widget
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText == englishGreeting?
                    spanishGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.filter_1_sharp)),
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText == englishGreeting?
                    thaiGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.filter_2_sharp)),
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText == englishGreeting?
                    frenchGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.filter_3_sharp))

          ],
        ),
        body: Center(
          child: Text(
              displayText,
              style: TextStyle(fontSize: 24)
          ),
        ),
      ),
    );
  }

        }